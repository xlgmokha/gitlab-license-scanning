module FixtureFileHelper
  def fixture_file_content(path)
    IO.read(License::Management.root.join("spec/fixtures/#{path}"))
  end
end
