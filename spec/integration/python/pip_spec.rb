require 'spec_helper'

RSpec.describe "pip" do
  context "when a project depends on the latest version of pip" do
    let(:requirements) { "sentry-sdk>=0.7.7" }

    it 'produces a valid report' do
      runner.add_file('requirements.txt', requirements)

      report = runner.scan

      expect(report).not_to be_empty
      expect(report).to match_schema(version: '2.0')
      expect(report[:version]).to start_with('2')
      expect(report[:dependencies].map { |x| x[:name] }).to include("sentry-sdk")
      expect(report[:dependencies].find { |x| x[:name] == 'sentry-sdk' }[:licenses]).to match_array(["BSD-4-Clause"])
    end
  end

  context "when the project has a dependency that depends on a minimum of python 3.6" do
    let(:requirements) do
      [
        'boto3',
        'aws-lambda-context>=1.0.0',
        'jsonschema>=3.0.0',
        'python-json-logger>=0.1.10',
        'sentry-sdk>=0.7.7',
        'https://s3-eu-west-1.amazonaws.com/new10-pypi/new10-logging-1.1.4.tar.gz',
        'ptvsd',
        'pylint',
        'flake8',
        'bandit',
        'pydocstyle'
      ].join("\n")
    end

    it 'produces a valid report' do
      runner.add_file('requirements.txt', requirements)

      report = runner.scan

      expect(report).not_to be_empty
      expect(report).to match_schema(version: '2.0')
      expect(report[:version]).to start_with('2')
      expect(report[:licenses]).not_to be_empty
      expect(report[:dependencies]).not_to be_empty
    end
  end
end
