# frozen_string_literal: true

module GitLab
  module License
    module Scanning
      VERSION = '2.7.0'
    end
  end
end
