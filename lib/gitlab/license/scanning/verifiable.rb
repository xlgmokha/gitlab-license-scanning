# frozen_string_literal: true

module GitLab
  module License
    module Scanning
      module Verifiable
        def blank?(item)
          item.nil? || item.empty?
        end

        def present?(item)
          !blank?(item)
        end
      end
    end
  end
end
