# frozen_string_literal: true

require 'pathname'
require 'yaml'
require 'json'
require 'license_finder'
require 'spandx'
require 'gitlab/license/scanning/loggable'
require 'gitlab/license/scanning/verifiable'
require 'gitlab/license/scanning/repository'
require 'gitlab/license/scanning/report'
require 'gitlab/license/scanning/version'

# This applies a monkey patch to the JsonReport found in the `license_finder` gem.
LicenseFinder::JsonReport.prepend(GitLab::License::Scanning::Report)

module GitLab
  module License
    module Scanning
      def self.root
        Pathname.new(File.dirname(__FILE__)).join('../../..')
      end

      def self.http
        @http ||= Net::Hippie::Client.new.tap do |client|
          client.logger = ::Logger.new('http.log')
          client.follow_redirects = 3
        end
      end
    end
  end
end
