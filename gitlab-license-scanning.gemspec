# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'gitlab/license/scanning/version'

Gem::Specification.new do |spec|
  spec.name          = 'gitlab-license-scanning'
  spec.version       = GitLab::License::Scanning::VERSION
  spec.authors       = ['Fabien Catteau', 'Olivier Gonzalez', 'mo khan']
  spec.email         = ['fcatteau@gitlab.com', 'ogonzalez@gitlab.com', 'mkhan@gitlab.com']

  spec.summary       = 'License Scanning job for GitLab CI.'
  spec.description   = 'License Scanning job for GitLab CI.'
  spec.homepage      = 'https://gitlab.com/gitlab-org/security-products/license-management'
  spec.license       = 'Nonstandard'

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/gitlab-org/security-products/license-management'
  spec.metadata['changelog_uri'] = 'https://gitlab.com/gitlab-org/security-products/license-management/blob/master/CHANGELOG.md'

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir.glob('exe/*') + Dir.glob('lib/**/**/*.{rb,yml}') + Dir.glob('*.{md,yml,json}')
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'license_finder', '~> 6.0.0'
  spec.add_dependency 'spandx', '~> 0.1'
  spec.add_development_dependency 'json-schema', '~> 2.8'
  spec.add_development_dependency 'rspec', '~> 3.9'
end
